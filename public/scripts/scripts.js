document.addEventListener('DOMContentLoaded', function(event){
    let boxes = document.getElementsByClassName("innerbox")
    for (let i=0; i < boxes.length; i++) {
        boxes[i].addEventListener('click', e => {
            addTimer(e.target)
        })
    }
})

let addEL = (element, e, func, args) => {
    element.addEventListener(e, () => {
        func(args) // will use an array for args if I need them
    })
}

let addTimer = (element) => {
    let origHtml = element.innerHTML
    let baseTime = 100000 //timer in ms
    let endTime = new Date().getTime() + baseTime
    setInterval(() => {
        let timeDifference = endTime - new Date().getTime()
        let timeLeft = Math.floor(timeDifference / 1000)
        element.innerHTML = `<span class="timer">${timeLeft}</span>`
        if (timeLeft < 0 ) {
            clearInterval(addTimer)
            element.innerHTML = origHtml
        }
    }
    , 1000)
}